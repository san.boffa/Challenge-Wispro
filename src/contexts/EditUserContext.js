import React, { createContext, useState } from "react";

const EditUserContext = createContext();

const EditUserProvider = ({ children }) => {
  const [selectedUser, setSelectedUser] = useState({});
  return (
    <EditUserContext.Provider value={{ selectedUser, setSelectedUser }}>
      {children}
    </EditUserContext.Provider>
  );
};

export { EditUserProvider };
export default EditUserContext;
