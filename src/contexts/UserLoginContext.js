import React, { createContext, useState } from "react";

const UserLoginContext = createContext();

const UserLoginProvider = ({ children }) => {
  const [user, setUser] = useState({});
  console.log(user);
  return (
    <UserLoginContext.Provider value={{ user, setUser }}>
      {children}
    </UserLoginContext.Provider>
  );
};

export { UserLoginProvider };
export default UserLoginContext;
