import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import Home from "./Pages/Home";
import Login from "./Pages/Login";

import { UserLoginProvider } from "./contexts/UserLoginContext";

const theme = {};

function App() {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <UserLoginProvider>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
          </Switch>
        </UserLoginProvider>
      </ThemeProvider>
    </Router>
  );
}

export default App;
