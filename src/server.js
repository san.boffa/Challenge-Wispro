const server = require("http").createServer();
const os = require("os-utils");

const io = require("socket.io")(server, {
  transports: ["websocket", "polling"],
});

let tick = 0;

io.on("connection", (client) => {
  setInterval(() => {
    os.cpuUsage((cpuPercent) => {
      client.emit("cpu", {
        name: tick++,
        value: cpuPercent,
      });
    });

    client.emit("memory", {
      name: tick++,
      value: Math.floor(Math.random() * 500 + 1000),
    });

    client.emit("internet", {
      name: tick++,
      value: Math.floor(Math.random() * 500 + 1000),
    });
  }, 1000);
});

server.listen(8080);
