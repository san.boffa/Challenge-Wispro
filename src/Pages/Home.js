import React, { useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import decodeJwt from "jwt-decode";

import UserList from "../Components/UserList/UserList";
import RealTimeGraphics from "../Components/RealTimeGraphics/RealTimeGraphics";
import Header from "../Components/Header";

import { EditUserProvider } from "../contexts/EditUserContext";
import UserLoginContext from "../contexts/UserLoginContext";

const Home = () => {
  const { setUser } = useContext(UserLoginContext);
  let history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (!token) {
      history.push("/login");
    } else {
      setUser(decodeJwt(token));
    }
  }, []);

  return (
    <>
      <Header />
      <section className="bg-gray-200">
        <RealTimeGraphics />
        <EditUserProvider>
          <UserList />
        </EditUserProvider>
      </section>
    </>
  );
};

export default Home;
