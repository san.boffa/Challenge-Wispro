import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import UserLoginContext from "../contexts/UserLoginContext";
import styled, { css } from "styled-components";

const StyledButton = styled.button`
  background-color: #2fafb8;
`;
const StyledSvg = styled.svg`
  color: #22777d;
`;

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  let history = useHistory();

  const handleLogin = (e) => {
    e.preventDefault();
    let errors = "";
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(`http://localhost:3040/users?email=${email}`, requestOptions)
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        const user = result[0];
        if (password === user.password) {
          if (user.token) {
            localStorage.setItem("token", user.token);
            history.push("/");
          } else {
            setError("User is not enabled");
          }
        } else {
          setError("Wrong password");
        }
      })
      .catch((error) => {
        setError("Wrong user");
      });
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img
            className="mx-auto h-12 w-auto"
            src="./logo.png"
            alt="Workflow"
          />
        </div>
        <form
          className="mt-8 space-y-6"
          action="#"
          onSubmit={(e) => handleLogin(e)}
        >
          <input type="hidden" name="remember" value="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email-address" className="sr-only">
                Email address
              </label>
              <input
                id="email-address"
                name="email"
                type="email"
                autoComplete="email"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Email address"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Password
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
            </div>
          </div>
          {error !== "" && (
            <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
              {error}
            </span>
          )}
          <div>
            <StyledButton
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <StyledSvg
                  className="h-5 w-5"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clipRule="evenodd"
                  />
                </StyledSvg>
              </span>
              Sign in
            </StyledButton>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
