import React, { useContext, useState } from "react";
import EditUserContext from "../../contexts/EditUserContext";

const UserForm = (props) => {
  const { selectedUser, setSelectedUser } = useContext(EditUserContext);
  const [error, setError] = useState({});

  const validationData = () => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const errors = {};
    if (!selectedUser.first_name) errors["nameError"] = "Name is required";
    if (!selectedUser.last_name)
      errors["lastNameError"] = "Last Name is required";
    if (!selectedUser.dni) errors["dniError"] = "Dni is required";
    if (!selectedUser.email) {
      errors["emailError"] = "Email is required";
    } else if (!re.test(String(selectedUser.email).toLowerCase()))
      errors["emailError"] = "The email is not in the correct format ";

    if (!selectedUser.password) {
      errors["passwordError"] = "Password is required";
    } else if (selectedUser.repPassword !== selectedUser.password) {
      errors["repeatPassowrdError"] = "The passwords do not match";
    }

    setError(errors);
  };

  const handleChange = (e) => {
    const tempSelectedUser = selectedUser;
    tempSelectedUser[e.target.id] = e.target.value;
    validationData();
    setSelectedUser({ ...tempSelectedUser });
  };

  return (
    <div className="mt-10 sm:mt-0">
      <form action="#" method="POST">
        <div className="shadow overflow-hidden sm:rounded-md">
          <div className="px-4 py-5 bg-white sm:p-6">
            <div className="grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="first_name"
                  className="block text-sm font-medium text-gray-700"
                >
                  First name
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="first_name"
                  id="first_name"
                  autoComplete="given-name"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker ${
                    error.nameError ? "border-red-500" : ""
                  }`}
                  value={selectedUser.first_name}
                />
                {error.nameError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.nameError}
                  </span>
                )}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="last_name"
                  className="block text-sm font-medium text-gray-700"
                >
                  Last name
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="last_name"
                  id="last_name"
                  autoComplete="family-name"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker ${
                    error.lastNameError ? "border-red-500" : ""
                  }`}
                  value={selectedUser.last_name}
                />
                {error.lastNameError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.lastNameError}
                  </span>
                )}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="dni"
                  className="block text-sm font-medium text-gray-700"
                >
                  DNI
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="dni"
                  id="dni"
                  autoComplete="DNI"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker ${
                    error.dniError ? "border-red-500" : ""
                  }}`}
                  value={selectedUser.dni}
                />
                {error.dniError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.dniError}
                  </span>
                )}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="email"
                  className="block text-sm font-medium text-gray-700"
                >
                  Email address
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="email"
                  id="email"
                  autoComplete="email"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker ${
                    error.emailError ? "border-red-500" : ""
                  }`}
                  value={selectedUser.email}
                />
                {error.emailError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.emailError}
                  </span>
                )}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="email"
                  className="block text-sm font-medium text-gray-700"
                >
                  Password
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="password"
                  name="password"
                  id="password"
                  autoComplete="password"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker ${
                    error.passwordError ? "border-red-500" : ""
                  }`}
                  value={selectedUser.password}
                />
                {error.passwordError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.passwordError}
                  </span>
                )}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="repPassword"
                  className="block text-sm font-medium text-gray-700"
                >
                  Repeat the Password
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="password"
                  name="repPassword"
                  id="repPassword"
                  autoComplete="repPassword"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker${"border-red-500"}`}
                  value={selectedUser.repPassword}
                />
                {error.repeatPassowrdError && (
                  <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                    {error.repeatPassowrdError}
                  </span>
                )}
              </div>

              <div className="col-span-6">
                <label
                  htmlFor="address"
                  className="block text-sm font-medium text-gray-700"
                >
                  Street address
                </label>
                <input
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="address"
                  id="address"
                  autoComplete="address"
                  className={`shadow w-full appearance-none border rounded py-2 px-3 text-grey-darker${"border-red-500"}`}
                  value={selectedUser.address}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default UserForm;
