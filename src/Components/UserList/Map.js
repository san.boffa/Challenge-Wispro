import React, { useRef, useEffect, useContext } from "react";
import { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
import EditUserContext from "../../contexts/EditUserContext";

const Map = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyB7Q8cm64CmqVIVFAAYtv_m88dF54IkKoc&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `450px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  const { selectedUser } = useContext(EditUserContext);
  const mapRef = useRef(null);

  useEffect(() => {
    if (selectedUser.latitud) {
      mapRef.current.panTo(
        new window.google.maps.LatLng(
          Number(selectedUser.latitud),
          Number(selectedUser.longitud)
        )
      );
    }
  }, [selectedUser]);

  return (
    <GoogleMap
      defaultZoom={3.7}
      defaultCenter={{
        lat: -38.416097,
        lng: -64.616672,
      }}
      ref={mapRef}
    >
      {props.users.length > 0 &&
        props.users.map((user, i) => {
          return (
            <Marker
              key={i}
              position={{
                lat: Number(user.latitud),
                lng: Number(user.longitud),
              }}
            />
          );
        })}
    </GoogleMap>
  );
});

export default Map;
