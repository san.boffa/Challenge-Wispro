import React from "react";

const Pagination = (props) => {
  const { setPage, page } = props;

  return (
    <ul className="flex justify-end m-4 ">
      <li
        className={`mx-1 px-3 py-2 rounded-lg bg-gray-200 ${
          page > 1
            ? "text-gray-700 hover:bg-gray-700 hover:text-gray-200"
            : "text-gray-500 cursor-not-allowed"
        }`}
      >
        <a
          className={`flex items-center font-bold ${
            page === 1 && "cursor-not-allowed"
          }`}
          href="#"
        >
          <span className="mx-1" onClick={() => setPage((prev) => prev - 1)}>
            Previous
          </span>
        </a>
      </li>
      {[...Array(5).keys()].map((e) => {
        page > 5 ? (e += 5) : (e += 1);
        return (
          <li
            key={e}
            className={`mx-1 px-3 py-2 hover:bg-gray-700 hover:text-gray-200 rounded-lg ${
              e === page
                ? "bg-indigo-500 text-gray-200"
                : "bg-gray-200 text-gray-700"
            } `}
            onClick={() => setPage(e)}
          >
            <a className="font-bold" href="#">
              {e}
            </a>
          </li>
        );
      })}
      <li className="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg">
        <a className="flex items-center font-bold" href="#">
          <span className="mx-1" onClick={() => setPage((prev) => prev + 1)}>
            Next
          </span>
        </a>
      </li>
    </ul>
  );
};

export default Pagination;
