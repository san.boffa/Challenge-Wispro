import React, { useState } from "react";
import styled, { css } from "styled-components";

const StyledInputContainer = styled.button`
  height: 65px;
`;

const StyledInput = styled.input`
  width: 300px;
`;

const Filter = (props) => {
  const [selectFilterName, setSelectFilterName] = useState("");
  const [selectFilterValue, setSelectFilterValue] = useState("");

  const handleSearch = () => {
    const queryFilter = selectFilterName + "_like=" + selectFilterValue;
    props.setFilters(queryFilter);
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  return (
    <div className="flex sm:justify-between sm:flex-row content-center m-4 justify-center flex-col items-center ">
      <StyledInputContainer className="px-4 py-3 bg-gray-50 text-right sm:px-6">
        <div className=" mt-1 relative rounded-md shadow-sm h-full">
          <StyledInput
            type="text"
            name="filter"
            id="filter"
            className="rounded-md pl-2 h-full"
            placeholder="Search..."
            onChange={(e) => setSelectFilterValue(e.target.value)}
            value={selectFilterValue}
            onKeyPress={handleKeyPress}
          />
          <div className="absolute inset-y-0 right-0 bg-white">
            <label htmlFor="FilterName" className="sr-only">
              {selectFilterName === "" ? "Filtros" : selectFilterName}
            </label>
            <select
              onChange={(e) => setSelectFilterName(e.target.value)}
              id="filterName"
              name="filterName"
              className="w-full h-full py-0 pl-2 pr-7 border-transparent bg-transparent text-gray-500 rounded-md"
              value={selectFilterName}
            >
              <option value={"first_name"}>Name</option>
              <option value={"last_name"}>Last Name</option>
              <option value={"email"}>Email</option>
              <option value={"dni"}>DNI</option>
              <option value={"creation_date"}>Creation Date</option>
              <option value={"address"}>Address</option>
            </select>
          </div>
        </div>
      </StyledInputContainer>
      <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
        <button
          onClick={handleSearch}
          className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Search
        </button>
      </div>
    </div>
  );
};

export default Filter;
