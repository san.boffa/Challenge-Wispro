import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

import styled, { css } from "styled-components";

const StyledContainer = styled.button`
  position: relative;
  display: flex;
`;

let arrayMeses = [];

const mes = new Date().getMonth() + 1;
let meses = mes + 1;

for (let i = 11; i >= 0; i--) {
  arrayMeses[i] = meses-- <= 1 ? meses + 12 : meses;
}

const objetoMeses = [
  {
    _id: 1,
    value: "January",
  },
  {
    _id: 2,
    value: "February",
  },
  {
    _id: 3,
    value: "March",
  },
  {
    _id: 4,
    value: "April",
  },
  {
    _id: 5,
    value: "May",
  },
  {
    _id: 6,
    value: "July",
  },
  {
    _id: 7,
    value: "Julio",
  },
  {
    _id: 8,
    value: "August",
  },
  {
    _id: 9,
    value: "September",
  },
  {
    _id: 10,
    value: "October",
  },
  {
    _id: 11,
    value: "November",
  },
  {
    _id: 12,
    value: "December",
  },
];

const GraficoEncuestas = (props) => {
  const dataEncuestas = () => {
    let array = [];

    for (let i = 6; i < 12; i++) {
      array[i - 6] = {
        name: objetoMeses.find((mes) => mes._id === arrayMeses[i]).value,
        Accesses: Math.floor(Math.random() * 100),
      };
    }

    return array;
  };

  return (
    <div className="mt-12 sm:mt-0">
      <div className="mr-8 mt-8">
        <StyledContainer>
          <LineChart data={dataEncuestas()} width={700} height={300}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="Accesses"
              stroke="#0088fe"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </StyledContainer>
      </div>
    </div>
  );
};

export default GraficoEncuestas;
