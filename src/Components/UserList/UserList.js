import React, { useState, useEffect, useContext } from "react";
import Modal from "../Modal";
import UserForm from "./UserForm";
import UserRow from "./UserRow";
import GraphCard from "./GraphCard";
import Pagination from "./Pagination";
import EditUserContext from "../../contexts/EditUserContext";
import Filter from "./Filters";
import Map from "./Map";

const UserList = () => {
  const [page, setPage] = useState(1);
  const [users, setUsers] = useState([]);
  const [showModalEditUser, setShowModalEditUser] = useState(false);
  const [showModalGraph, setShowModalGraph] = useState(false);
  const { selectedUser, setSelectedUser } = useContext(EditUserContext);
  const [filters, setFilters] = useState("");

  useEffect(() => {
    //TODO refactor
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      `http://localhost:3040/users?_page=${page}&${filters}`,
      requestOptions
    )
      .then((response) => {
        console.log(response.headers.get("Link"));
        return response.json();
      })
      .then((result) => setUsers(result))
      .catch((error) => console.log("error", error));
  }, [page, filters]);

  const handleSaveUser = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify(selectedUser);

    var requestOptions = {
      method: "PATCH",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`http://localhost:3040/users/${selectedUser.id}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        setSelectedUser({});
        const tempUsers = users;
        const indexUserEdit = tempUsers.findIndex(
          (user) => user.id === result.id
        );
        tempUsers[indexUserEdit] = result;
        setUsers([...tempUsers]);
      })
      .catch((error) => console.log("error", error));
  };

  const handleDeleteUser = (user) => {
    var requestOptions = {
      method: "DELETE",
      redirect: "follow",
    };

    fetch(`http://localhost:3040/users/${user.id}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        const tempUsers = users;
        const indexUserEdit = tempUsers.findIndex((e) => user.id === e.id);
        console.log(tempUsers[indexUserEdit]);
        tempUsers.splice(indexUserEdit, 1);
        setUsers([...tempUsers]);
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <>
      <div className="flex flex-col m-4 bg-gray-50">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block w-full sm:px-6 lg:px-8">
            <Map users={users} selectedUser={selectedUser} />
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <Filter setFilters={setFilters} />
            </div>
            <table className=" divide-y divide-gray-200 w-full">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Name
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Last Name
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Email
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    DNI
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Creation Date
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Address
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Status
                  </th>

                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th>
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Delete</span>
                  </th>
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Used</span>
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {users.map((user, i) => (
                  <UserRow
                    key={i}
                    setShowModalEditUser={setShowModalEditUser}
                    setShowModalGraph={setShowModalGraph}
                    user={user}
                    handleDeleteUser={handleDeleteUser}
                  />
                ))}
              </tbody>
            </table>
            <Pagination page={page} setPage={setPage} />
          </div>
        </div>
      </div>

      {showModalEditUser && (
        <Modal
          setShowModal={setShowModalEditUser}
          content={<UserForm />}
          title="Edit User"
          sendRequest={handleSaveUser}
          optionSave
        />
      )}
      {showModalGraph && (
        <Modal
          setShowModal={setShowModalGraph}
          content={<GraphCard />}
          title="Amount of accesses per day in the last week"
        />
      )}
    </>
  );
};

export default UserList;
