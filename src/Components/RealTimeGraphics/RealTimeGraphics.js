import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { LineChart, Line, XAxis, YAxis } from "recharts";
import styled, { css } from "styled-components";

const StyledChart = styled(LineChart)`
  margin-left: -35px;
  margin-top: 10px;
`;

const socket = io("http://localhost:8080", {
  transports: ["websocket", "polling"],
});

const initialValue = Array(20).fill({
  name: "",
  value: 0,
});

const RealTimeGraphics = () => {
  const [dataCpu, setDataCpu] = useState(initialValue);
  const [dataMemory, setDataMemory] = useState(initialValue);
  const [dataInternet, setDataInternet] = useState(initialValue);

  // 1. listen for a cpu event and update the state
  useEffect(() => {
    socket.on("cpu", (data) => {
      setDataCpu((currentData) => {
        if (currentData.length > 20) {
          currentData.shift();
        }
        return [...currentData, data];
      });
    });
    socket.on("memory", (data) => {
      setDataMemory((currentData) => {
        if (currentData.length > 20) {
          currentData.shift();
        }
        return [...currentData, data];
      });
    });
    socket.on("internet", (data) => {
      setDataInternet((currentData) => {
        if (currentData.length > 20) {
          currentData.shift();
        }
        return [...currentData, data];
      });
    });
  }, []);

  return (
    <div className="flex items-center bg-gray-200 text-gray-800">
      <div className="p-4 w-full">
        <div className="grid grid-cols-12 gap-4">
          <div className="col-span-12 md:col-span-4 ">
            <div className="flex flex-row bg-white shadow-sm rounded-lg p-4">
              <div className="flex items-center justify-center flex-shrink-0 h-12 w-12 rounded-xl bg-blue-100 text-blue-500">
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                  ></path>
                </svg>
              </div>
              <div className="flex flex-col flex-grow ml-4">
                <div className="font-bold text-lg">%CPU</div>
                <StyledChart data={dataCpu} width={500} height={200}>
                  <XAxis dataKey="name" />
                  <YAxis domain={[0, 1]} />
                  <Line dataKey="value" />
                </StyledChart>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-4 ">
            <div className="flex flex-row bg-white shadow-sm rounded-lg p-4">
              <div className="flex items-center justify-center flex-shrink-0 h-12 w-12 rounded-xl bg-green-100 text-green-500">
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"
                  ></path>
                </svg>
              </div>
              <div className="flex flex-col flex-grow ml-4">
                <div className="text-sm text-gray-500">400 Mb</div>
                <StyledChart data={dataMemory} width={500} height={200}>
                  <XAxis dataKey="name" />
                  <YAxis />
                  <Line dataKey="value" />
                </StyledChart>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-4 ">
            <div className="flex flex-row bg-white shadow-sm rounded-lg p-4">
              <div className="flex items-center justify-center flex-shrink-0 h-12 w-12 rounded-xl bg-orange-100 text-orange-500">
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"
                  ></path>
                </svg>
              </div>
              <div className="flex flex-col flex-grow ml-4">
                <div className="font-bold text-lg">Internet consumption</div>
                <StyledChart data={dataInternet} width={500} height={200}>
                  <XAxis dataKey="name" />
                  <YAxis />
                  <Line dataKey="value" />
                </StyledChart>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RealTimeGraphics;
