import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import UserLoginContext from "../contexts/UserLoginContext";

const Header = (props) => {

  const { user, setUser } = useContext(UserLoginContext);
  const [openMenu, setOpenMenu] = useState(false);
  
  let history = useHistory();

  return (
    <div className="relative bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <a href="#">
              <span className="sr-only">Workflow</span>
              <img className="h-8 w-auto sm:h-10" src="./logo.png" alt="" />
            </a>
          </div>

          <div className="flex items-center">
            <div className="flex-shrink-0 h-10 w-10">
              <img
                className="h-10 w-10 rounded-full"
                src={user.avatar}
                alt=""
              />
            </div>
            <div
              className="ml-4"
              onClick={() => {
                setOpenMenu((prev) => !prev);
              }}
            >
              <div className="text-sm font-medium text-gray-900">
                {user.first_name}
              </div>
              <div className="text-sm text-gray-500">{user.email}</div>
            </div>

            {openMenu && (
              <div class="absolute z-10 mt-42 transform px-2 sm:px-0 lg:ml-0 mt-32">
                <div
                  onClick={() => {
                    localStorage.removeItem("token");
                    setUser({});
                    history.push("/login");
                  }}
                  class="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden"
                >
                  <div class="relative bg-white px-12 py-6">
                    <a
                      href="#"
                      class="-p-3 flex items-start rounded-lg hover:bg-gray-50"
                    >
                      <p class="text-base font-medium text-gray-900">Log Out</p>
                    </a>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
