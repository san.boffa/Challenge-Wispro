# Wispro Challenge
 
### `npm install`
 
Instala los paquetes necesarios.
 
### `npm run start`
 
Inicializa la aplicación en http://localhost:3000/.
 
Las credenciales para loguearse son:
 
User: s@s.com
Password:123456
 
### `npm run server`
 
Inicializa el server de prueba en http://localhost:3040/.
 
### `npm run socket-serve`
 
Inicializa el websocket para obtener los datos de la CPU, Memoria y Uso internet en http://localhost:8080/.

